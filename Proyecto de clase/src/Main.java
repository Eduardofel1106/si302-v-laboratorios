public class Main {

    public static void mostrarReporteNotasBajas(int[] notas, int cantidadMostrar) {
        for (int i = 0; i < notas.length; i++) {
            for (int j = i + 1; j < notas.length; j++) {
                if (notas[i] < notas[j]) {
                    int aux = notas[i];
                    notas[i] = notas[j];
                    notas[j] = aux;
                }
            }
        }
        for (int i = 0; i < cantidadMostrar; i++) {
            System.out.println(notas[notas.length - i - 1])
        }
    }

    public static void main(String[] args) {
        int [] notas = {12, 14, 11, 8, 19, 13, 17, 10, 5};
        mostrarReporteNotasBajas(notas,3);
    }
}
